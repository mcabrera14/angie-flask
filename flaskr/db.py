import sqlite3

import click
from flask import current_app, g 
# g special object is unique for each request

def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db

def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def init_db():
    db = get_db()
    #open a file realative the the flaskr package, get _db returns a database connection, used to excute the commands
    #read from the file
    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

#click.command defines the command line called init-db that calls the init_db function 
#and shows a success message to the user
@click.command('init-db')
def init_db_command():
    #Clear the existing data and creat new tables
    init_db()
    click.echo('Initialize the datatbase')


    
def init_app(app):
    #tell Flask to call that function when cleaning up after returning the response
    app.teardown_appcontext(close_db)
    #adds new command that can be called with the Flask command
    app.cli.add_command(init_db_command)

